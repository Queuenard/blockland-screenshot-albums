#!/bin/bash

mkdir .public

deploy_dir()
{
	echo -e "$1\t$2\t$3"
	album_short_name="$1"
	archive_org_path="$2"
	album_git_repo="$3"

	mkdir ".public/$album_short_name"

	cp $album_short_name/METADATA.js html_pages/*.js html_pages/*.html ".public/$album_short_name/"

	sed -i "s/{ALBUM_SHORT_NAME}/${album_short_name//\//\\/}/g" .public/$album_short_name/*.html
	sed -i "s/{ARCHIVE_ITEM_NAME}/${archive_org_path//\//\\/}/g" .public/$album_short_name/*.html
	sed -i "s/{ALBUM_GIT_REPO}/${album_git_repo//\//\\/}/g" .public/$album_short_name/*.html
}

while read -r line
do
	album_short_name="$(echo "$line" | cut -f1 -d$'\t')";
	archive_org_path="$(echo "$line" | cut -f2 -d$'\t')";
	album_git_repo="$(echo "$line" | cut -f3 -d$'\t')";
	deploy_dir $album_short_name $archive_org_path $album_git_repo
done < .albums

mv .public public


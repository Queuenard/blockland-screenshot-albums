var DATABASE_HEAD = null;
var DATABASE_TAIL = null;
var DATABASE_JSON = null;

var USE_INTERNAL = 0;

if((""+ window.location).startsWith("file:///"))
	USE_INTERNAL = 1;

function encodeHTMLEntities(text)
{
	var tempArea = document.createElement('temparea');
	tempArea.innerText = text;
	return tempArea.innerHTML;
}

function init()
{
	if(page == "viewer")
	{
		if(USE_INTERNAL == 0)
		{
			//hide some fields
			document.getElementById('BG_REVIEW').remove();
			document.getElementById('BG_PUBLISH').remove();
			document.getElementById('ROW_REVIEW_NOTE').remove();

			document.getElementById('TD_LINK_VIEWER').remove();

			//not necessary
			document.getElementById('DISPLAY_YEAR_MONTH').colSpan = 3;
		}
	}

	else if(page == "search")
	{
		//nothing to do
	}



	if(USE_INTERNAL == 0)
	{
		eval("init_page_"+ page).call();
	}

	else
	{
		var new_script = document.createElement('script');
		new_script.onload = function() { eval("init_page_"+ page).call(); };
		new_script.async = true;
		new_script.setAttribute('src', 'METADATA_INTERNAL.js');
		document.head.appendChild(new_script);
	}
}

function init_database()
{
	cols = get_metadata_json_cols();

	if(USE_INTERNAL == 1)
		DATABASE_JSON = get_metadata_json_INTERNAL();
	else
		DATABASE_JSON = get_metadata_json();

	if(DATABASE_JSON.length > 0)
	{
		DATABASE_HEAD = DATABASE_JSON[0];
		DATABASE_TAIL = DATABASE_JSON[DATABASE_JSON.length - 1];
	}

	var prev_obj = null;
	for(var i = 0; i < DATABASE_JSON.length; i++)
	{
		var obj = DATABASE_JSON[i];
		obj.index = i;

		for(var f = 0; f < cols.length; f++)
		{
			obj[cols[f]] = null;

			if(obj[f] != null)
				obj[cols[f]] = obj[f];
		}

		obj.next = null;
		obj.prev = null;

		if(i != 0)
		{
			obj.prev = prev_obj;
			prev_obj.next = obj;
		}

		prev_obj = obj;
	}
}

function init_page_viewer()
{
	init_database();

	onHashChange(window.location.hash);

	document.getElementById('href_FIRST').href = "#img_"+ DATABASE_JSON[0].BASENAME;
	document.getElementById('href_LAST').href = "#img_"+ DATABASE_JSON[DATABASE_JSON.length-1].BASENAME;

	if(window.location.hash.length == 0)
		loadImage(DATABASE_JSON[0]);
}

function init_page_search()
{
	init_database();

	var input = document.getElementById("search_text");

	input.addEventListener("keyup", function(event) {
		if (event.keyCode === 13) {
			//event.preventDefault();
			document.getElementById("search_button").click();
		}
	});
}


function search_clicked()
{
	var search_text = document.getElementById('search_text').value;

	var tbody = document.getElementById('SEARCH_TABLE').getElementsByTagName('tbody')[0];

	var count = tbody.childElementCount;
	for(var i = 0; i < count; i++)
	{
		tbody.deleteRow(0);
	}

	if(search_text.length == 0)
		return;

	for(var i = 0; i < DATABASE_JSON.length; i++)
	{
		var search_text_lwr = search_text.toLowerCase();

		var bool_search_date = document.getElementById('bool_search_date').checked;
		var bool_search_basename = document.getElementById('bool_search_basename').checked;
		var bool_search_title = document.getElementById('bool_search_title').checked;
		var bool_search_desc = document.getElementById('bool_search_desc').checked;
		var bool_search_serv = document.getElementById('bool_search_server').checked;
		var bool_search_players = document.getElementById('bool_search_players').checked;

		var match = 0;

		//TODO: any means it doesn't need to check anything else
		if(bool_search_date && (""+ DATABASE_JSON[i].YEAR_MONTH).toLowerCase().includes(search_text_lwr))
			match = 1;

		if(bool_search_basename && (""+ DATABASE_JSON[i].BASENAME).toLowerCase().includes(search_text_lwr))
			match = 1;

		if(bool_search_title && (""+ DATABASE_JSON[i].NAME).toLowerCase().includes(search_text_lwr))
			match = 1;

		if(bool_search_serv && (""+ DATABASE_JSON[i].SERVER).toLowerCase().includes(search_text_lwr))
			match = 1;

		if(bool_search_desc && (""+ DATABASE_JSON[i].DESC).toLowerCase().includes(search_text_lwr))
			match = 1;

		if(bool_search_players && (""+ DATABASE_JSON[i].PLAYERS).toLowerCase().includes(search_text_lwr))
			match = 1;


		if(!match)
			continue;

		
		var row = tbody.insertRow();

		var cell_date = row.insertCell();
		var cell_basename = row.insertCell();
		var cell_title = row.insertCell();

		if(USE_INTERNAL && DATABASE_JSON[i].PUBLISH)
		{
			if(DATABASE_JSON[i].PUBLISH == 2)
				cell_basename.style.backgroundColor = "#FFFF00";
			else
				cell_basename.style.backgroundColor = "#00FF00";
		}

		cell_date.innerHTML=DATABASE_JSON[i].YEAR_MONTH;
		cell_basename.innerHTML='<a href="viewer.html#img_'+ DATABASE_JSON[i].BASENAME +'">'+ DATABASE_JSON[i].BASENAME +'</a>';
		
		var filtered_name = encodeHTMLEntities(DATABASE_JSON[i].NAME);
		if(DATABASE_JSON[i].NAME != null)
			cell_title.innerHTML='<a href="viewer.html#img_'+ DATABASE_JSON[i].BASENAME +'">'+ filtered_name +'</a>';
	}
}



/*
window.addEventListener('hashchange', function()
{
	alert("loc changed!");
});*/

window.onhashchange = function() {
	onHashChange(window.location.hash);
};

function onHashChange(hash)
{
	if(!hash)
		return;

	if(hash.startsWith("#img_"))
	{
		gotoImageByName(hash.substring("#img_".length));
		return;
	}

	//else
	//	alert(hash);
}

function gotoImageByName(name)
{
	for(var i = 0; i < DATABASE_JSON.length; i++)
	{
		if(DATABASE_JSON[i].BASENAME == name)
		{
			loadImage(DATABASE_JSON[i]);
			window.location.hash = "img_"+ DATABASE_JSON[i].BASENAME;
			return;
		}
	}

	document.getElementById('BASENAME_ERROR').innerHTML = "<p style=\"color:red; background-color: black;\">NOT FOUND: "+ name +"</p>";
}

function gotoImageRandom()
{
	num = Math.floor(Math.random() * DATABASE_JSON.length);

	gotoImageByName(DATABASE_JSON[num].BASENAME);
}

function loadImage(json)
{
	document.getElementById('DISPLAY_COUNTER').innerHTML = (json.index+1) +" / "+ DATABASE_JSON.length;
	document.getElementById('DISPLAY_YEAR_MONTH').innerHTML = json.YEAR_MONTH;

	document.getElementById('DISPLAY_NAME').innerHTML = encodeHTMLEntities(json.NAME);
	document.getElementById('DISPLAY_SERVER').innerHTML = encodeHTMLEntities(json.SERVER);
	document.getElementById('DISPLAY_DESC').innerHTML = encodeHTMLEntities(json.DESC);
	document.getElementById('DISPLAY_BASENAME').innerHTML = json.BASENAME;
	document.getElementById('BASENAME_ERROR').innerHTML = "";


	document.getElementById('ARCHIVE_URL').href = 
		"https://archive.org/details/"+ get_archive_org_path() +"/screenshots/"
		+ json.YEAR_MONTH +"/"+ json.BASENAME;

	document.getElementById('TD_LINK_ARCHIVE').style.display = "";
	document.getElementById('ARCHIVE_URL').innerHTML = "Archive Link";

	//IMPORTANT: unload the image first
	document.getElementById('DISPLAY_IMG').src = "";

	if(USE_INTERNAL)
	{
		document.getElementById('DISPLAY_REVIEW_NOTE').innerHTML = json.REVIEW_NOTE;
		document.getElementById('DISPLAY_REVIEW').innerHTML = json.REVIEW;
		document.getElementById('DISPLAY_PUBLISH').innerHTML = json.PUBLISH;
		document.getElementById('BG_PUBLISH').style.backgroundColor = "";

		document.getElementById('DISPLAY_IMG').src = json.ABS_PATH;
		document.getElementById('DISPLAY_IMG_HREF').href = json.ABS_PATH;

		if(json.REVIEW == 1)
			document.getElementById('BG_REVIEW').style.backgroundColor = "#FFFF00";

		else
		{
			document.getElementById('BG_REVIEW').style.backgroundColor = "#00FF00";
			document.getElementById('DISPLAY_PUBLISH').innerHTML = json.PUBLISH;
		}

		if(json.PUBLISH == 0)
		{
			document.getElementById('TD_LINK_ARCHIVE').style.display = "none";
			document.getElementById('TD_LINK_VIEWER').style.display = "none";
		}
		else
		{
			document.getElementById('TD_LINK_ARCHIVE').style.display = "";
			document.getElementById('TD_LINK_VIEWER').style.display = "";

			document.getElementById('LIVE_VIEWER_LINK').href = 
				get_html_file_paths() +"viewer.html#img_"+ json.BASENAME;
			document.getElementById('LIVE_VIEWER_LINK').innerHTML = "Live Viewer";

			document.getElementById('BG_PUBLISH').style.backgroundColor = "#00FF00";
		}
	}
	else
	{
		document.getElementById('DISPLAY_IMG').src =
		"https://archive.org/download/"+ get_archive_org_path() +"/screenshots/"
		+ json.YEAR_MONTH +"/"+ json.BASENAME;

		document.getElementById('DISPLAY_IMG_HREF').href =
		"https://archive.org/download/"+ get_archive_org_path() +"/screenshots/"
		+ json.YEAR_MONTH +"/"+ json.BASENAME;
	}




	document.getElementById('DISPLAY_PLAYERS').innerHTML = "";

	var color='00FF00';
	if(json.PLAYERS == null)
		json.PLAYERS = "";
	var playerList = json.PLAYERS.split("\n");

	for(var i = 0; i < playerList.length; i++)
	{
		var terminator = ' - ';
		if(i == playerList.length - 1)
			terminator = '';

		if(color == '00FF00')
			color = 'FF00FF';
		else
			color = '00FF00';

		playerList[i] = encodeHTMLEntities(playerList[i]);
		playerList[i] = playerList[i].replace(' ', '&nbsp;');

		document.getElementById('DISPLAY_PLAYERS').innerHTML +=
			'<span style="color: #'+ color +'">'+ playerList[i] +'</span>'+ terminator;
	}


	var element = document.getElementById('href_prev');
	if(json.prev != null)
		element.href = "#img_"+ json.prev.BASENAME;
	else
		element.removeAttribute('href');

	var element = document.getElementById('href_next');
	if(json.next != null)
		element.href = "#img_"+ json.next.BASENAME;
	else
		element.removeAttribute('href');

}



window.onload = function()
{
	init();
}



